#include <iostream>
#include <fstream> //For file output and input
#include <istream>
#include <string> //Needed to use getline
#include <cstring>

//#include <cassert>
//#include <ctime>

using namespace std;

//CONSTANTS
const int MAXRESULTS = 20; // Max matches that can be found
const int MAXDICTWORDS = 30000; // Max words that can be read in

//DECLARATIONS REQUIERED FUNCTIONS:
int vocabularyCreator(istream& dictfile, string dict[]);
int potentialSequences(string word, const string dict[], int size, string results[]);
void outcomeDisclosure(const string results[], int size);


//HELPER FUNCTIONS
void clearArray(string arr[], int & n);

void outcomeDisclosureHelper(const string results[], int & size);

int vocabularyCreatorHelper(istream& dictfile, int& count, string dict[]);

int getPermutations(string& prefix, string& rest,
    const string dict[], const int& size, string results[], int& addedWords, bool& endProcess);

void Loop(int& i, string& prefix, string& rest,
    const string dict[], const int& size, string results[], int& addedWords, bool& endProcess);

//LOOPS TO CHECK FOR WORD IN DICT AND REPETITIONS IN RESULTS
bool CheckInDict(const string& word, const string dict[], const int& size,
    int& addedWords, bool& endProcess);

bool wordNotInResults(int & j, const string& word, string results[], const int& addedWords);

bool wordInDict(int & i, const int& size, const string& word, const string dict[], const int& addedWords);



//IMPLEMENTATIONS:
int vocabularyCreatorHelper(istream& dictfile, int & count, string dict[]) 
{
    string line;
    if (count >= MAXDICTWORDS)
        return count; // Retornar la cuenta actual 

    if (!getline(dictfile, line))
        return count; // Retornar la cuenta actual si se alcanza el final

    dict[count] = line; 

    count++;
    return vocabularyCreatorHelper(dictfile, count, dict);
}

int vocabularyCreator(istream& dictfile, string dict[]) 
//NOTICE: the reference to the ifstream dictfile is an istream&
{
    if (dict[0] != "") {
        int max(MAXDICTWORDS);
        clearArray(dict, max);
    }
    int count(0);
    return vocabularyCreatorHelper(dictfile, count, dict);
}

bool CheckInDict(const string & word, const string dict[], const int & size, 
    int & addedWords, bool & endProcess)
{
    if (addedWords >= MAXRESULTS)
    {
        endProcess = true; //Flag to know if results is full, to stop the remaining process
        return false;
    }

    int iterator(0);
    return wordInDict(iterator, size, word, dict, addedWords);
}

bool wordInDict(int & i, const int & size, const string& word, const string dict[], const int& addedWords)
{
    if (i >= size)
        return false;

    if (dict[i] == word)
    {
        return true;
    }
    i++;
    return wordInDict(i, size, word, dict, addedWords);
}

bool wordNotInResults(int & j, const string & word, string results[], const int& addedWords)
{
    if (j >= addedWords)
        return true;
    if (results[j] == word)
        return false;

    j++;
    return wordNotInResults(j, word, results, addedWords);
}

int getPermutations(string & prefix, string & rest, 
    const string dict[], const int & size, string results[], int& addedWords, bool & endProcess)
{
    //Fix the first letter and recombine the next letters until no letters are left
    //Fix the next letter and repeat the above
    //Do the same with all letters
    if (endProcess == true)
        return addedWords;

    if (prefix.length() == 0 && rest.length() == 0)
        return 0;
    if (rest.length() == 0)
    {
        //time_t t = std::time(0);
        //tm now;
        //localtime_s(&now, &t);
        //cerr << now.tm_hour << ':' << now.tm_min << ':' << now.tm_sec << " - ";
        //cerr << prefix << endl;
        int temp(size), iterator(0);
        if (CheckInDict(prefix, dict, temp, addedWords, endProcess) && wordNotInResults(iterator, prefix, results, addedWords))
        {
            results[addedWords] = prefix;
            addedWords++;
        }

    }

    int i(0);
    Loop(i, prefix, rest, dict, size, results, addedWords, endProcess);

    return addedWords;
}

void Loop(int & i, string & prefix, string & rest,
    const string dict[], const int & size, string results[], int& addedWords, bool & endProcess)
{
    if (endProcess == true)
        return;
    if (i >= rest.length())
        return;

    string newPrefix = prefix + rest[i];
    string newRest = rest.substr(0, i) + rest.substr(i + 1);

    getPermutations(newPrefix, newRest, dict, size, results, addedWords, endProcess);

    i++;
    Loop(i, prefix, rest, dict, size, results, addedWords, endProcess);
}

int potentialSequences(string word, const string dict[], int size, string results[])
{
    if (results[0] != "") {
        int max(MAXRESULTS);
        clearArray(results, max);
    }

    string prefix("");
    int addedWords(0);
    bool endProcess(false);
    return getPermutations(prefix, word, dict, size, results, addedWords, endProcess);
}

void outcomeDisclosure(const string results[], int size)
{
    outcomeDisclosureHelper(results, size);
}

void outcomeDisclosureHelper(const string results[], int & size)
{
    if (size <= 0)
        return;

    cout << "Matching word " << results[0] << endl;

    size--;
    outcomeDisclosureHelper(results + 1, size);
}

void clearArray(string arr[], int & n) 
{
    if (n <= 0) return;
    arr[0] = "";
    n--;
    clearArray(arr + 1, n);
}

int main()
{
    string results[MAXRESULTS];
    string dict[MAXDICTWORDS];

    ifstream dictfile; // file containing the list of words
    dictfile.open("words30k.txt");
    if (!dictfile) {
        cout << "File not found!" << endl;
        return (1);
    }

    int nwords; // number of words read from dictionary
    string word;

    nwords = vocabularyCreator(dictfile, dict);

    cout << "Please enter a string for an anagram: ";
    cin >> word;

    int numMatches = potentialSequences(word, dict, nwords, results);
    if (!numMatches) {
        cout << "No matches found" << endl;
    }
    else {
        outcomeDisclosure(results, numMatches);
    }


     return 0;

}

